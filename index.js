const dotenv = require('dotenv')
const express = require("express")
const path = require("path");

var app = express()
const mongoose = require('mongoose');
const User = require('./models/user');
dotenv.config();

app.listen(3000, function () {
    console.log("Started application on port %d", 3000)
});

mongoose.connect('mongodb://' + process.env.HOST_IP + ':' + process.env.MONGO_PORT + '/devOpsDB')
    .then(() => {
        console.log('CONNECTION OPEN');
    }).catch(err => {
        console.log('Connection Error!');
        console.log(err);
    });

//======================= Pre Setup ===================================
app.use(express.static(path.join(__dirname, '/public')));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));


//======================== Code Development ===========================

app.get("/", function (req, res) {
    res.render('Users/index');
})

app.get("/users", async function (req, res) {
    let users = await User.find({});
    res.render('Users/listing', { users });
})

app.get("/users/view/:id", async (req, res) => {
    const { id } = req.params;
    const obj = await User.findById(id);
    res.render('Users/view', { obj })
})

app.get("/users/add", (req, res) => {
    res.render('Users/add');
})