const mongoose = require('mongoose');
const User = require('./models/user');

mongoose.connect('mongodb://localhost:27017/devOpsDB')
    .then(() => {
        console.log('CONNECTION OPEN');
    }).catch(err => {
        console.log('Connection Error!');
        console.log(err);
    });

const userSchema = new mongoose.Schema({
    userCode: { type: String, maxlength: 100, required: true },
    userName: { type: String, maxlength: 250, required: true },
    password: { type: String, maxlength: 250, required: true },
    firstName: { type: String, maxlength: 250 },
    lastName: { type: String, maxlength: 250 },
    gender: { type: String, enum: ['M', 'F'], required: true },
    email: { type: String, maxlength: 350 },
    userStatus: { type: String, maxlength: 50, required: true },
    userGroup: { type: String, maxlength: 100 },
    photo: { type: String, maxlength: 1000 },
    phoneNum: { type: String, maxlength: 50 },
    userCrea: { type: String, maxlength: 100, required: true },
    dateCrea: { type: Date, required: true },
    userUpdt: String,
    dateUpdt: Date
});

const Users = mongoose.model('user', userSchema);
const newUser = new Users({ userCode: 'Admin', userName: 'Administrator', password: '123', firstName: 'Samnang', lastName: 'Chheng', gender: 'M', email: 'admin@mail.com', userStatus: 'active', userGroup: 'Admin', photo: 'N/A', phoneNum: '070893164', userCrea: 'Admin', dateCrea: Date.now() });

newUser.save().then(p => {
    console.log(p);
}).catch(e => {
    console.log(e);
});