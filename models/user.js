const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    userCode: { type: String, maxlength: 100, required: true },
    userName: { type: String, maxlength: 250, required: true },
    password: { type: String, maxlength: 250, required: true },
    firstName: { type: String, maxlength: 250 },
    lastName: { type: String, maxlength: 250 },
    gender: { type: String, enum: ['M', 'F'], required: true },
    email: { type: String, maxlength: 350 },
    userStatus: { type: String, maxlength: 50, required: true },
    userGroup: { type: String, maxlength: 100 },
    photo: { type: String, maxlength: 1000 },
    phoneNum: { type: String, maxlength: 50 },
    userCrea: { type: String, maxlength: 100, required: true },
    dateCrea: { type: Date, required: true },
    userUpdt: String,
    dateUpdt: Date
});

const User = mongoose.model('User', userSchema);
module.exports = User;